module Global (
    module Global.Class
  , gAccessToken
  , gGen
  , gOrderCheckStarted
  , gUpdateStreamStatStarted
  ) where

import Import
import Global.Class
import System.IO.Unsafe
import System.Random.MWC

gAccessToken :: IORef String
{-# NOINLINE gAccessToken #-}
gAccessToken = unsafePerformIO $ newIORef ""

-- createSystemRandom is a somewhat expensive function, and is intended to be called only
-- occasionally (e.g. once per thread). You should use the Gen it creates to generate many
-- random numbers.
gGen :: GenIO
{-# NOINLINE gGen #-}
gGen = unsafePerformIO createSystemRandom

gOrderCheckStarted :: MVar Bool
{-# NOINLINE gOrderCheckStarted #-}
gOrderCheckStarted = unsafePerformIO $ newMVar False

gUpdateStreamStatStarted :: MVar Bool
{-# NOINLINE gUpdateStreamStatStarted #-}
gUpdateStreamStatStarted = unsafePerformIO $ newMVar False
