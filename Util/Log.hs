{-# Language CPP #-}
{-# LANGUAGE OverloadedStrings #-}

module Util.Log where

-- TODO (later): implement verbose control

-- DEBUG       - debug messages
-- INFO        - information
-- NOTICE      - normal runtime conditions
-- WARNING     - general warnings
-- ERROR       - general errors
-- CRITICAL    - severe situations
-- ALERT       - take immediate action
-- EMERGENCY   - system is unusable

import Control.Concurrent
import Control.Exception
import Data.Time
import Import hiding (try, threadDelay)
import Network.Socket
import System.Directory
import System.Log.Formatter
import System.Log.Handler hiding (setLevel)
import System.Log.Handler.Simple
import System.Log.Logger (rootLoggerName, setHandlers, setLevel,
                          updateGlobalLogger, Priority(..))
import qualified Data.ByteString.Char8 as B
import qualified System.Log.Logger as L

self :: SockAddr
self = SockAddrInet 0 0

debugM :: String -> SockAddr -> ByteString -> IO ()
debugM l a = logM' l DEBUG a

infoM :: String -> SockAddr -> ByteString -> IO ()
infoM l a = logM' l INFO a

noticeM :: String -> SockAddr -> ByteString -> IO ()
noticeM l a = logM' l NOTICE a

warningM :: String -> SockAddr -> ByteString -> IO ()
warningM l a = logM' l WARNING a

errorM :: String -> SockAddr -> ByteString -> IO ()
errorM l a = logM' l ERROR a

criticalM :: String -> SockAddr -> ByteString -> IO ()
criticalM l a = logM' l CRITICAL a

alertM :: String -> SockAddr -> ByteString -> IO ()
alertM l a = logM' l ALERT a

emergencyM :: String -> SockAddr -> ByteString -> IO ()
emergencyM l a = logM' l EMERGENCY a

logM' :: String -> Priority -> SockAddr -> ByteString -> IO ()
logM' logger priority addr msg = logM logger priority addr msg

logM :: String -> Priority -> SockAddr -> ByteString -> IO ()
logM logger priority addr msg =
  case msg of
    "" -> return ()
    _  -> let m = if addr == self
                    then B.unpack msg
                    else concat ["[", show addr, "] ", B.unpack msg]
          in L.logM logger priority m

logInit :: String -> IO ()
logInit dir = do
  createDirectoryIfMissing True $ "log/" ++ dir
  logUpdate dir

-- TODO (later): disable streamHandler for deployment
logUpdate :: String -> IO ()
logUpdate dir = do
#if DEVELOPMENT
  putStrLn $ "logger dir: " <> pack dir
  hs   <- mapM (fmap logFormatter) [streamHandler stderr DEBUG]
#else
  path <- logFilePath dir
  hs   <- mapM (fmap logFormatter) [fileHandler   path   DEBUG]
#endif
  updateGlobalLogger rootLoggerName (setLevel DEBUG . setHandlers hs)

logFilePath :: String -> IO FilePath
logFilePath dir = do
  t <- getCurrentTime
  z <- getCurrentTimeZone -- %F is same as %Y-%m-%d
  let date = formatTime defaultTimeLocale "%F" $ utcToLocalTime z t
  return $ concat ["log/", dir, "/", date, ".log"]

logFormatter :: GenericHandler Handle -> GenericHandler Handle
logFormatter handler = setFormatter handler f
  where f = simpleLogFormatter "[$time : $loggername : $prio] $msg"

-- This is for use with refreshLogHandlers only: notice the threadDelay.
logRotate :: String -> FilePath -> IO ()
logRotate dir path = do
  threadDelay (10 * 60 * 1000000) -- 10min
  path' <- logFilePath dir
  if path' == path then logRotate dir path else do
    logUpdate dir -- test shows that the old fd will be closed
    logRotate dir path'

refreshLogHandlers :: String -> IO ()
refreshLogHandlers dir = do
  path     <- logFilePath dir
  (Left e) <- try (logRotate dir path)
  unless (fromException e == Just ThreadKilled) (refreshLogHandlers dir)

setLoggerLevel :: Priority -> String -> IO ()
setLoggerLevel p logger = updateGlobalLogger logger (setLevel p)

loggerList :: [String]
loggerList = ["OLD", "ALARM", "CONN", "SYS", "CLI", "MQTT", "OSS", "DB"]

disableLoggers :: IO ()
disableLoggers = mapM_ (setLoggerLevel EMERGENCY) loggerList

enableLogger :: ByteString -> IO ()
enableLogger logger = setLoggerLevel DEBUG (B.unpack logger)
