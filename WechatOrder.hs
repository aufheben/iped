module WechatOrder where

import Prelude
import Database.Persist.TH

data OrderStatus = OrderCreated | OrderPaid | OrderClosed | OrderFailed
    deriving (Show, Read, Eq)
derivePersistField "OrderStatus"
