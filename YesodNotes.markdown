```
$newline never
```

$newline always: Put newlines between consecutive text lines, and between tags.
$newline text: Put newlines between consecutive text lines, but not between tags.
$newline never: Never put in newlines automatically.

https://github.com/yesodweb/yesod/wiki/1.1-upgrade

```
<html class="no-js" lang="en">
```

When Modernizr runs, it removes the "no-js" class and replaces it with "js".
This is a way to know in your CSS whether or not Javascript support is enabled.

http://stackoverflow.com/questions/6724515/what-is-the-purpose-of-the-html-no-js-class
