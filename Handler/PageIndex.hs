module Handler.PageIndex where

import Import
import Handler.Util
import Text.Printf

getPageIndexR :: Handler Html
getPageIndexR = do
  (username, cameras)  <- getUsernameCameras
  Just (Entity _ user) <- runDB $ getBy $ UniqueUsername username
  let balance = printf "%.2f" (userAccountBalance user) :: String
  mmsg <- getMessage
  pc   <- widgetToPageContent $(widgetFile "page-index")
  withUrlRenderer [hamlet| ^{pageBody pc} |]
