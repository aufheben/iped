module Handler.WechatPayNotify where

import Import
import Data.Time
import Handler.Util
import Text.Hamlet.XML
import Text.XML
import Text.XML.Cursor
import Wechat (payKey)
import WechatOrder
import qualified Data.Map.Lazy as M
import qualified Data.Text.Lazy as T

postWechatPayNotifyR :: Handler T.Text
postWechatPayNotifyR = do
  doc <- rawRequestBody $$ sinkDoc def -- TODO: MonadThrow
  let cursor   = fromDocument doc
      -- if an element doesn't exist, val_of returns "" because T.concat [] == ""
      val_of e = concat $ cursor $/ element e &// content
      ret_sucs = return $ render_text root_success
      ret_fail = return $ render_text root_fail
      ret_shit = $logError "Shit happened" >> ret_sucs -- yes, ret_sucs
  -- Verify sign
  --
  -- TODO: a better way to get names? and why doesn't child or descendant work for v instead
  -- of anyElement?
  let k = map ((\(NodeElement e) -> nameLocalName $ elementName e) . node) (cursor $/ anyElement)
      v = cursor $/ anyElement &// content
      ([(_, sign)], kv) = partition (\(a, _) -> a == "sign") (sort $ zip k v)
      sign' = uncurry signKV (unzip $ kv ++ [("key", payKey)])
  if sign /= sign'
    then $logError "Incorrect sign for Wechat pay result!" >> ret_fail
    else
      case val_of "return_code" of
        "SUCCESS" ->
          case val_of "result_code" of
            "SUCCESS" -> do
              let [out_trade_no, time_end] = map val_of ["out_trade_no", "time_end"]
              runDB $ do
                entity <- getBy $ UniqueOrderId out_trade_no
                case entity of
                  Just (Entity order_key o) ->
                    if wechatOrderStatus o /= OrderCreated
                      then $logWarn "order already processed"
                      else orderPaid time_end order_key o
                  Nothing -> $logError "out_trade_no is not in database!"
              ret_sucs
            _ -> ret_shit
        _ -> ret_shit
        -- TODO: set order status to OrderFailed when shit happens
  where
  render_text  = toXmlStr renderText

  root_success = Element "xml" M.empty [xml|
                   <return_code>SUCCESS
                   <return_msg>OK
                 |]

  root_fail    = Element "xml" M.empty [xml|
                   <return_code>FAIL
                   <return_msg>MISERABLY
                 |]

orderPaid :: MonadIO m => Text -> Key WechatOrder -> WechatOrder -> ReaderT SqlBackend m ()
orderPaid time_end order_key order = do
  -- time_end is the time the user finished the purchase, and it's
  -- always in Beijing time
  let start     = parseTimeOrError True defaultTimeLocale "%Y%m%d%H%M%S"
                                   (unpack time_end) :: LocalTime
      start_utc = localTimeToUTC beijingTZ start
      diff_time = fromIntegral $ 60 * 60 * 24 * (wechatOrderDays order)
      end_utc   = addUTCTime diff_time start_utc
  update order_key [WechatOrderStatus =. OrderPaid,
                    WechatOrderStart  =. start_utc,
                    WechatOrderEnd    =. end_utc]
