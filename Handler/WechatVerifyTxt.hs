module Handler.WechatVerifyTxt where

import Import

getWechatVerifyTxtR :: Text -> Handler ()
getWechatVerifyTxtR txt_file = sendFile "text/plain" $ "/root/wechat/" <> unpack txt_file
