module Handler.AdminDevices where

import Import
import Handler.Util

getAdminDevicesR :: Handler Html
getAdminDevicesR = do
  pc <- widgetToPageContent $(widgetFile "admin-devices")
  withUrlRenderer [hamlet| ^{pageBody pc} |]

-- Camera added by admin will have password in clear text, but after it is bound by a user,
-- the password will be hashed or set to empty (because it's not used afterwords).
postAdminDevicesR :: Handler Html
postAdminDevicesR = do
  dev_id  <- runInputPost $ ireq textField "id"
  maybe_c <- runDB $ getBy $ UniqueDevId dev_id
  case maybe_c of
    Nothing -> do
      -- TODO: validate ID (8-bytes, numbers)
      password' <- genString 8
      stream_id <- genString 8
      utc_time  <- liftIO getCurrentTime
      let password = encodeUtf8 password'
      _ <- runDB $ insert $ Camera dev_id "我的设备" "" "" "vga" "" password ""
                                   stream_id False utc_time utc_time "" 0 0 0 False
      successAlert $ "绑定密码: " <> password'
    Just _  -> failureAlert $ dev_id <> "已存在"

-- TODO: one day we shall drop the 'page' part in the URL:
-- /dashboard/devices/ID, /admin/devices/ID
getAdminDeviceInfoR :: Text -> Handler Html
getAdminDeviceInfoR dev_id = do
  maybe_c <- runDB $ getBy $ UniqueDevId dev_id
  case maybe_c of
    Nothing           -> failureAlert $ dev_id <> "不存在"
    Just (Entity _ c) -> do
      -- TODO: table
      let Camera _ name _ _ _ username password appid _ _ _ _ _ 0 _ _ _ = c
      successAlert $ dev_id <> ": name " <> name <> " username " <> username <> " password " <>
                     decodeUtf8 password <> " appid " <> appid
