module Handler.WechatPay where

import Handler.Util
import Import

-- Because this URL is fixed as a "directory", we must pass those values as query parameters,
-- otherwise, just put them in the URL would be more convenient (and semantically not wrong,
-- if not completely correct), as we did for WechatUnifiedOrderR
getWechatPayR :: Handler Html
getWechatPayR = do
  let params = ["appid", "appname", "devid", "devname", "fee"]
  [appid, appname, devid, devname, fee] <- catMaybes <$> mapM lookupGetParam params
  wechatLayout $ do
    let fee_val = toValue [("fee", fee)]
    setTitle $ toHtml appname
    $(widgetFile "wechat-pay")
