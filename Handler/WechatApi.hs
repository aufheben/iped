module Handler.WechatApi where

import Import hiding (hash)
import Crypto.Hash
import qualified Data.Text as T

-- This verifies the server is indeed Wechat server. By simply returning the echostr,
-- we are connected to the Wechat Public Platform.
getWechatApiR :: Handler Html
getWechatApiR = do
  [s, t, n, e] <- catMaybes <$> mapM lookupGetParam ["signature", "timestamp", "nonce", "echostr"]
  let s' = pack . show . sha1 . encodeUtf8 . concat . sort $ [token, t,  n]
  if s' == s then return (toHtml e) else error "A man doesn't trust a server"
  where
  token = "PpOx5IXytxK7dVIYHRdubMAxWp20KZOK"
  sha1  = hash :: ByteString -> Digest SHA1

postWechatApiR :: Handler Html
postWechatApiR = do
  return (toHtml T.empty)
