module Handler.PagePublish where

import Import
import Data.Aeson
import Handler.Util
import qualified Data.Text as T

getPagePublishR :: Handler Html
getPagePublishR = do
  (_, cameras) <- getUsernameCameras
  pc <- widgetToPageContent $(widgetFile "page-publish")
  withUrlRenderer [hamlet| ^{pageBody pc} |]

postPagePublishR :: Handler Html
postPagePublishR = do
  maid <- maybeAuthId
  case maid of
    Nothing -> redirect $ AuthR LoginR
    Just _  -> do
      result <- parseJsonBody :: Handler (Result [Text])
      case result of
        Error s         -> failureAlert (T.pack s)
        Success id_list -> do
          -- TODO: (setMessage "发布成功") doesn't work?
          mapM_ publish_enable id_list
          return "ok"
  where
  publish_enable id_ = do
    runDB $ do
      maybe_c  <- getBy $ UniqueDevId id_
      case maybe_c of
        Nothing                   -> return ()
        Just (Entity camera_id _) -> update camera_id [CameraStreamEnabled =. True]

      -- id_      <- runInputPost $ ireq textField "id"
      -- maybe_c  <- runDB $ getBy $ UniqueDevId id_
      -- case maybe_c of
      --   Nothing           -> defaultLayout [whamlet|<p>设备不存在|]
      --   Just (Entity _ c) -> do
      --     let rtmp s   = "rtmp://" <> srsServerAddress <> ":1935/live/" <> s :: Text
      --         hls  s   = "http://" <> srsServerAddress <> ":8080/live/" <> s <> ".m3u8" :: Text
      --         streamId = cameraStreamId c
      --     defaultLayout [whamlet|
      --       <p>视频发布成功
      --       <p>RTMP地址：#{rtmp streamId}
      --       <p>HLS地址：#{hls streamId}
      --     |]
