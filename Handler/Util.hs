module Handler.Util where

import Import hiding (Element, hash)
import Crypto.Hash
import Data.UUID
import Data.UUID.V4
import Data.Time.LocalTime
import Global
import System.Random.MWC
import Text.XML
import qualified Data.ByteString as BW
import qualified Data.HashMap.Strict as M

mqttServerAddress :: Text
mqttServerAddress = "80.85.85.186:3000"

srsServerAddress :: Text
srsServerAddress = "80.85.85.186"

rtmpAddr, hlsAddr :: Text -> Text
rtmpAddr s = "rtmp://" <> srsServerAddress <> ":1935/live/" <> s
hlsAddr  s = "http://" <> srsServerAddress <> ":8080/live/" <> s <> ".m3u8"

genUUID_ASCII :: IO ByteString
genUUID_ASCII = fmap toASCIIBytes nextRandom

genString :: MonadIO m => Int -> m Text
genString len = do
  str <- liftIO $ replicateM len (to_num <$> uniformR (97, 132) gGen) -- 'a' to 'z' plus 10
  return . decodeUtf8 . BW.pack $ str
  where
  to_num n = if n > 122 then n - 75 else n

genInt :: MonadIO m => Int -> m Int
genInt len = liftIO $ uniformR (0, 10 ^ len - 1) gGen

toValue :: [(Text, Text)] -> Value
toValue = toJSON . M.fromList

toXmlStr :: IsString a => (RenderSettings -> Document -> a) -> Element -> a
toXmlStr render_func root =
  render_func def { rsUseCDATA = const True } $ Document (Prologue [] Nothing []) root []

signKV :: [Text] -> [Text] -> Text
signKV k v = toUpper . pack . show . md5 . encodeUtf8 $ url_kv
  where
  url_kv = intercalate "&" . map (\(a, b) -> a <> "=" <> b) $ zip k v
  md5    = hash :: ByteString -> Digest MD5

beijingTZ :: TimeZone
beijingTZ = hoursToTimeZone 8

beijingTime :: UTCTime -> Text
beijingTime t = pack $ formatTime defaultTimeLocale "%Y%m%d%H%M%S" $ utcToLocalTime beijingTZ t

successAlert :: MonadHandler m => Text -> m Html
successAlert msg = withUrlRenderer [hamlet|
  <div .alert .alert-block .alert-success>
    <button type="button" .close data-dismiss="alert">
      <i .ace-icon .fa .fa-times>
    <i .ace-icon .fa .fa-check .green> #{msg}
  |]

failureAlert :: MonadHandler m => Text -> m Html
failureAlert msg = withUrlRenderer [hamlet|
  <div .alert .alert-block .alert-danger>
    <button type="button" .close data-dismiss="alert">
      <i .ace-icon .fa .fa-times>
    <i .ace-icon .fa .fa-warning .red> #{msg}
  |]

-- scary type given by GHC
getUsernameCameras :: (YesodAuth master, RedirectUrl master (Route App),
               YesodPersist master, AuthId master ~ Text,
               YesodPersistBackend master ~ SqlBackend) =>
              HandlerT master IO (Text, [Camera])
getUsernameCameras = do
  maid <- maybeAuthId
  case maid of
    Nothing       -> redirect $ AuthR LoginR--TODO: maybe weird when this happens, test this case
    Just username -> do
      cameras <- runDB $ selectList [CameraBindUsername ==. username] []
      return $ (username, map entityVal cameras)
