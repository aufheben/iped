module Handler.Admin where

import Import

getAdminR :: Handler Html
getAdminR = do
  maid <- maybeAuthId
  case maid of
    Nothing       -> redirect $ AuthR LoginR
    Just username ->
      dashboardLayout $ do
        $(widgetFile "admin")
