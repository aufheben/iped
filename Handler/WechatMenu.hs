module Handler.WechatMenu where

import Import
import Global
import Network.HTTP.Simple
import Wechat (appId)

postWechatMenuR :: Handler Text
postWechatMenuR = do
  token <- readG gAccessToken
  req' <- parseUrlThrow (token_url token)
  let req = req' { method         = "POST"
                 , requestHeaders = [("Content-Type", "application/json")]
                 , requestBody    = RequestBodyBS (encodeUtf8 body)
                 }
  resp <- httpLBS req
  $logDebug body
  return (decodeUtf8 . toStrict . getResponseBody $ resp)
  where
  token_url = ("https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" <>)

  body  = concat ["{ \"button\":[", menu1, ",", menu2, "] }"]
  menu1 = "{\"type\":\"view\", \"name\":\"我的账户\", \"url\":\""<> authUrl mylivestream <>"\"}"
  menu2 = "{\"type\":\"view\", \"name\":\"直播列表\", \"url\":\""<> authUrl livestreams  <>"\"}"

authUrl :: ByteString -> Text
authUrl url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" <>
              appId <> "&redirect_uri=" <> decodeUtf8 (urlEncode False url) <>
              "&response_type=code&scope=snsapi_base&state=" <> appId <> "#wechat_redirect"

livestreams, mylivestream :: ByteString
livestreams  = "http://www.lighted.com.cn/wechat/livestreams"
mylivestream = "http://www.lighted.com.cn/wechat/mylivestream"
