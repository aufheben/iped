module Handler.WechatView where

import Import
import Data.Time.LocalTime
import Handler.Util
import WechatOrder

getWechatViewR :: Text -> Text -> Text -> Text -> Text -> Text -> Handler Html
getWechatViewR appid appname dev_id devname streamId orderid = do
  sess <- lookupSession "openid"
  case sess of
    Nothing     -> error "A user hasn't authorized"
    Just openid -> do
      let is_public = orderid == "public"
          player    = $(widgetFile "player")
      if is_public
        then
          wechatLayout $ do
            setTitle $ toHtml appname
            let (from_time, to_time) = ("", "") :: (Text, Text)
            $(widgetFile "wechat-view")
        else do
          order <- entityVal <$> runDB (getBy404 $ UniqueOrderId orderid)
          if wechatOrderAppId order  /= appid  ||
             wechatOrderOpenId order /= openid ||
             wechatOrderDevId order  /= dev_id ||
             wechatOrderStatus order /= OrderPaid
            then
              error "Something fishy is going on!" -- TODO: does this get logged?
            else
              wechatLayout $ do
                setTitle $ toHtml appname
                let format = formatTime defaultTimeLocale "%y-%m-%d" . utcToLocalTime beijingTZ
                    from_time = format $ wechatOrderStart order
                    to_time   = format $ wechatOrderEnd order
                $(widgetFile "wechat-view")
