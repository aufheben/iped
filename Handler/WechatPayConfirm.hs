module Handler.WechatPayConfirm where

import Import
import Handler.WechatMenu
import qualified Data.HashMap.Strict as M

getWechatPayConfirmR :: Handler Html
getWechatPayConfirmR = do
  Just prepay_id <- lookupGetParam "prepay_id"
  wechatLayout $ do
    let livestreams_url = toJSON $ M.fromList [("livestreams" :: Text, authUrl livestreams)]
    $(widgetFile "wechat-pay-confirm")
