{-# LANGUAGE DeriveGeneric #-}

module Handler.WechatAccount where

import Import
import Handler.Util
import Wechat

getWechatAccountR :: Handler Html
getWechatAccountR = error "Not yet implemented: getWechatAccountR"

-- TODO: is it possible to derive FromJSON directly for WechatAccount from config/models
data WeAccount = WeAccount { app_id    :: Text
                           , app_name  :: Text
                           , admin_key :: Text
                           }
  deriving Generic

instance FromJSON WeAccount

postWechatAccountR :: Handler Value
postWechatAccountR = do
  a <- requireJsonBody :: Handler WeAccount
  if admin_key a /= adminKey
    then permissionDenied "incorrect admin_key"
    else do
      void . runDB $ upsert (WechatAccount (app_id a) (app_name a)) []
      return $ toValue [("result", "OK")]
