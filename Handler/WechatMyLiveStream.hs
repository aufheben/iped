module Handler.WechatMyLiveStream where

import Import
import Handler.WechatLiveStreams
import WechatOrder
import qualified Data.HashMap.Strict as M

getWechatMyLiveStreamR :: Handler Html
getWechatMyLiveStreamR = do
  (appid, openid) <- wechatAuthorize
  (appname, cameras, orders) <- runDB $ do
    _            <- insertBy $ WechatUser openid
    Entity _ app <- getBy404 $ UniqueAppId appid
    cameras      <- map entityVal <$> selectList [CameraWechatAppId ==. appid] []
    let appname    = wechatAccountAppName app
    -- select orders paid by the user under this appid
    let filters = [WechatOrderOpenId ==. openid, WechatOrderAppId ==. appid,
                   WechatOrderStatus ==. OrderPaid]
    orders <- map entityVal <$> selectList filters [Desc WechatOrderEnd]
    return $ (appname, cameras, orders)
  now <- liftIO $ getCurrentTime
  let user_orders = processOrders now orders
  $logDebug $ pack $ show user_orders
  wechatLayout $ do
    setTitle $ toHtml appname
    $(widgetFile "wechat-mylivestream")
