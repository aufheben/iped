module Handler.WechatUnifiedOrder where

import Import hiding (keys, Element)
import Data.Aeson (encode)
import Data.Text.Read (decimal)
import Data.Time.Clock.POSIX
import Handler.Util
import Handler.WechatOrderQuery
import Network.Wai (remoteHost)
import Text.Hamlet.XML
import Text.Printf
import Text.XML
import Text.XML.Cursor
import Wechat
import WechatOrder
import qualified Data.Map.Lazy as M

isTest :: Bool
isTest = True

notifyUrl :: Text
notifyUrl = "http://www.lighted.com.cn/wechat/pay/notify"

returnErr :: Text -> Handler Value
returnErr err = return $ toValue [("error", err)]

-- These parameters are put in the URL, which is ok in our case. However, see below for `days`.
getWechatUnifiedOrderR :: Text -> Text -> Text -> Text -> Handler Value
getWechatUnifiedOrderR appid appname devid devname = do
  sess <- lookupSession "openid"
  case sess of
    Nothing     -> error "A user hasn't authorized"
    Just openid -> do
      now <- liftIO getCurrentTime
      -- From our test: currectly wechat reports error on SIGNERROR (for which return_code
      -- is FAIL) and ORDERPAID (for which return_code is SUCCESS but result_code is FAIL).
      -- As for time_start and time_expire, they don't work (as least not in our tests), and
      -- we think wechat relies on us to close the order when they expire. Finally,
      -- OUT_TRADE_NO_USED is bullshit, we can submit multiple times, obtaining a shiny new
      -- prepay_id each time.
      (fee, open_order, paid_order) <- runDB $ do
        Just (Entity _ cam) <- getBy $ UniqueDevId devid
        let filters     = [WechatOrderAppId ==. appid, WechatOrderOpenId ==. openid,
                           WechatOrderDevId ==. devid]
            filter_open = [WechatOrderStatus ==. OrderCreated]
            filter_paid = [WechatOrderStatus ==. OrderPaid, WechatOrderEnd >. now]
        open_order <- selectList (filter_open ++ filters) []
        paid_order <- selectList (filter_paid ++ filters) []
        return $ (cameraStreamFee cam, open_order, paid_order)
      case paid_order of
        [] ->
          case open_order of
            []  -> createUnifiedOrder appid appname devid devname openid now fee
            [o] -> payRequest $ wechatOrderPrepayId $ entityVal o
            x   -> error $ "More than 1 open orders exist: " <> pack (show x)
        _ -> returnErr "订单已支付"

createUnifiedOrder :: Text -> Text -> Text -> Text -> Text -> UTCTime -> Int -> Handler Value
createUnifiedOrder appid appname devid devname openid now fee = do
  ip      <- fmap (pack . takeWhile (/= ':') . show . remoteHost) waiRequest
  nonce   <- genString 32
  orderid <- genOrderId
  -- `days` is determined when the user pays, so it's more convenient to pass as a query str
  Just days <- lookupGetParam "days"
  let Right (days', _) = decimal days
  -- generate body and attach information
  let body        = genBody appname devname days
      attach      = genAttach appid devid days
      total_fee   = if isTest then "1" else pack $ show (days' * fee)
      trade_type  = "JSAPI"
  -- the keys and values to be signed
  let keys = ["appid", "attach", "body", "mch_id", "nonce_str", "notify_url", "openid",
              "out_trade_no", "spbill_create_ip", "total_fee", "trade_type", "key"] -- sorted
      vals = [appId, attach, body, mchId, nonce, notifyUrl, openid,
              orderid, ip, total_fee, trade_type, payKey]
  -- the XML together with the sign
  let root = Element "xml" M.empty [xml|
               <appid>#{appId}
               <mch_id>#{mchId}
               <nonce_str>#{nonce}
               <body>#{body}
               <attach>#{attach}
               <out_trade_no>#{orderid}
               <total_fee>#{total_fee}
               <spbill_create_ip>#{ip}
               <notify_url>#{notifyUrl}
               <trade_type>#{trade_type}
               <openid>#{openid}
               <sign>#{signKV keys vals}
             |]
  -- the order to be created in database without the prepay_id
  let partial_order = WechatOrder orderid appid openid devid now now now days' OrderCreated
  -- finally, get prepay_id and return the parameters for pay request
  getPrepayId root partial_order

genBody :: Text -> Text -> Text -> Text
genBody appname devname days = intercalate " " [appname, devname, months]
  where
  months = case days of
             "30"  -> "1个月"
             "90"  -> "3个月"
             "180" -> "6个月"
             _     -> days <> "天"

-- For now, attach is not used
genAttach :: Text -> Text -> Text -> Text
genAttach appid devid days = decodeUtf8 . toStrict . encode . toValue $ kv
  where
  kv = [("appid", appid), ("devid", devid), ("days", days)]

genOrderId :: MonadIO m => m Text
genOrderId = liftIO $ do
  t <- getCurrentTime
  i <- pack . printf "%016d" <$> genInt 16
  return $ beijingTime t <> i

getPrepayId :: Element -> (Text -> WechatOrder) -> Handler Value
getPrepayId root partial_order =
  postXml order_url root $ \doc -> do
    let cursor   = fromDocument doc
        val_of e = concat $ cursor $/ element e &// content
    -- if an element doesn't exist, val_of returns "" because T.concat [] == ""
    case val_of "return_code" of
      "SUCCESS" ->
        case val_of "result_code" of
          "SUCCESS" -> do
              -- 1. we didn't check sign here, meh
              -- 2. we still make assumptions, e.g. prepay_id must exist. we are fine with it
              let prepay_id = val_of "prepay_id"
              runDB $ insert_ $ partial_order prepay_id
              payRequest prepay_id
          _ -> returnErr $ val_of "err_code_des"
      _ -> returnErr $ val_of "return_msg"
      -- By _ we actually mean "FAIL", because that's the only possible value (we hope).
      --
      -- Maybe catching exception could lead to more concise code here, but that also
      -- leads to non-exhaustive pattern matching
  where
  order_url  = "https://api.mch.weixin.qq.com/pay/unifiedorder"

-- According to the documentation, timestamp is of +8 timezone. However this is clearly bullshit,
-- because in their official PHP SDK, timestamp was simply obtained by calling PHP's
-- time() function.
--
-- By the way, generating this on the server side is also the official approach.
payRequest :: Text -> Handler Value
payRequest prepay_id = do
  nonce   <- genString 32
  posix_t <- liftIO getPOSIXTime
  let timestamp  = pack $ show (round posix_t :: Int)
      keys       = ["appId", "nonceStr", "package", "signType", "timeStamp", "key"] -- sorted
      vals       = [appId, nonce, "prepay_id=" <> prepay_id, "MD5", timestamp, payKey]
      sign       = signKV keys vals
  return $ toValue (zip keys vals ++ [("paySign", sign)])
