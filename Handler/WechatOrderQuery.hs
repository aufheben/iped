module Handler.WechatOrderQuery where

import Import hiding (keys, Element)
import Handler.Util
import Handler.WechatPayNotify
import Network.HTTP.Simple
import Text.Hamlet.XML
import Text.XML
import Text.XML.Cursor
import Wechat
import WechatOrder
import qualified Data.Map.Lazy as M

-- Wechat doc says we should query 10 times before FAIL. But screw the docs.
getWechatOrderQueryR :: Text -> Handler Value
getWechatOrderQueryR prepay_id = do
  order' <- runDB $ selectFirst [WechatOrderPrepayId ==. prepay_id] []
  case order' of
    Nothing               -> error "prepay_id doesn't exist!"
    Just (Entity _ order) ->
      case wechatOrderStatus order of
        OrderCreated -> do
          status <- wechatOrderQuery $ wechatOrderOrderId order
          case status of
            OrderPaid -> return_val res_succ
            _         -> return_val res_fail
        OrderPaid -> return_val res_succ
        _         -> return_val res_fail
  where
  return_val = return . toValue
  res_succ   = [("result", "SUCCESS")]
  res_fail   = [("result", "FAIL")]

wechatOrderQuery :: Text -> Handler OrderStatus
wechatOrderQuery order_id = do
  root <- genRoot order_id
  postXml query_url root $ \doc -> do
    let cursor   = fromDocument doc
        val_of e = concat $ cursor $/ element e &// content
    case val_of "return_code" of
      "SUCCESS" ->
        case val_of "result_code" of
          "SUCCESS" -> do
            let status = val_of "trade_state"
            $logInfo $ status <> ": " <> order_id
            case status of
              "SUCCESS"    -> do
                let time_end = val_of "time_end"
                orderIO order_id $ \order_key o ->
                  when (wechatOrderStatus o == OrderCreated) $ do
                    $logInfo $ "set order status to OrderPaid: " <> order_id
                    orderPaid time_end order_key o
                return OrderPaid
              "REFUND"     -> order_fail
              "NOTPAY"     -> wechatOrderClose order_id >> setOrderStatus order_id OrderClosed
              "CLOSED"     -> setOrderStatus order_id OrderClosed
              -- "REVOKED" doesn't concern us
              -- "USERPAYING" shouldn't happen, so we return fail as well
              "USERPAYING" -> order_fail
              "PAYERROR"   -> order_fail
              _            -> order_fail
          _ -> $logWarn (val_of "err_code_des") >> order_fail
      _ -> $logWarn (val_of "return_msg") >> order_fail
  where
  query_url  = "https://api.mch.weixin.qq.com/pay/orderquery"
  order_fail = setOrderStatus order_id OrderFailed

orderIO :: Text -> (Key WechatOrder -> WechatOrder -> SqlPersistT Handler ()) -> Handler ()
orderIO order_id io =
  runDB $ do
    entity <- getBy $ UniqueOrderId order_id
    case entity of
      Nothing -> $logError $ "order doesn't exist: " <> order_id
      Just (Entity order_key o) -> io order_key o

setOrderStatus :: Text -> OrderStatus -> Handler OrderStatus
setOrderStatus order_id status = do
  orderIO order_id $ \order_key _ -> do
    $logInfo $ "set order status to " <> pack (show status) <> ": " <> order_id
    update order_key [WechatOrderStatus =. status]
  return status

wechatOrderClose :: Text -> Handler ()
wechatOrderClose order_id = do
  root <- genRoot order_id
  postXml close_url root $ \doc -> do
    let cursor   = fromDocument doc
        val_of e = concat $ cursor $/ element e &// content
    case val_of "return_code" of
      "SUCCESS" ->
        case val_of "result_code" of
          "SUCCESS" -> $logInfo $ "order closed: " <> order_id
          _ -> $logWarn $ val_of "err_code_des"
      _ -> $logWarn $ val_of "return_msg"
  where
  close_url = "https://api.mch.weixin.qq.com/pay/closeorder"

genRoot :: MonadIO m => Text -> m Element
genRoot order_id = do
  nonce <- genString 32
  -- the keys and values to be signed
  let keys = ["appid", "mch_id", "nonce_str", "out_trade_no", "key"] -- sorted
      vals = [appId, mchId, nonce, order_id, payKey]
  -- the XML together with the sign
  let root = Element "xml" M.empty [xml|
               <appid>#{appId}
               <mch_id>#{mchId}
               <nonce_str>#{nonce}
               <out_trade_no>#{order_id}
               <sign>#{signKV keys vals}
             |]
  return root

postXml :: (MonadIO m, MonadThrow m, MonadLogger m) => String -> Element -> (Document -> m b) -> m b
postXml url root io = do
  req' <- parseUrlThrow url
  -- the HTTP request
  let req_body = toXmlStr renderLBS root
      req = req' { method         = "POST"
                 , requestHeaders = [("Content-Type", "application/xml")]
                 , requestBody    = RequestBodyLBS req_body
                 }
  -- $logDebug $ decodeUtf8 (toStrict req_body)
  resp <- httpLBS req
  -- $logDebug $ decodeUtf8 (toStrict $ getResponseBody resp)
  case parseLBS def $ getResponseBody resp of
    Left e    -> error $ pack $ show e
    Right doc -> io doc
