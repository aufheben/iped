{-# LANGUAGE DeriveGeneric #-}

module Handler.PageId where

import Data.Time.LocalTime
import Import
import Handler.Util
import Network.HTTP.Simple
import Text.Printf

data ClientInfo = ClientInfo { ip :: Text, version :: Text }
  deriving (Generic, Show)

instance FromJSON ClientInfo

getPageIdR :: Text -> Handler Html
getPageIdR dev_id = do
  maid <- maybeAuthId
  case maid of
    Nothing       -> redirect $ AuthR LoginR
    Just username -> do
      maybe_c <- runDB $ getBy $ UniqueDevId dev_id
      case maybe_c of
        Nothing -> page_err "该设备不存在"
        Just (Entity camera_id camera) ->
          if cameraBindUsername camera /= username
            then page_err "该设备未绑定"
            else do
              let streamId = cameraStreamId camera
                  player   = $(widgetFile "player")
                  url      = "http://" <> mqttServerAddress <>
                             "/admin/clients/" <> dev_id <> "/json"
              stats <- map entityVal <$> runDB (
                         selectList [StreamStatCamera ==. camera_id]
                                    [LimitTo 24, Desc StreamStatTime])
              req  <- parseUrlThrow (unpack url)
              resp <- httpJSON req
              -- TODO: HTTP error
              let info = getResponseBody resp :: ClientInfo
                  mqtt = toValue [("mqtt", mqttServerAddress)]
                  dev  = toValue [("dev", dev_id)]
                  -- TODO: we are leaking password here
                  -- fake = L.replicate (length $ cameraWlanKey camera) '*'

              let show_time t = formatTime defaultTimeLocale "%F %T" $ utcToLocalTime beijingTZ t
                  show_mb b   = printf "%.2f" ((fromIntegral b :: Double) / 1000000.0) :: String
                  show_cost c = printf "%.2f" c :: String
              pc <- widgetToPageContent $(widgetFile "page-id")
              withUrlRenderer [hamlet| ^{pageBody pc} |]
  where
  page_err :: Text -> Handler Html
  page_err err = do
    pc <- widgetToPageContent $(widgetFile "page-err")
    withUrlRenderer [hamlet| ^{pageBody pc} |]
