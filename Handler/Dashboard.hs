module Handler.Dashboard where

import Import
import Handler.Util
-- import Yesod.Auth.Account

getDashboardR :: Handler Html
getDashboardR = do
  (username, cameras) <- getUsernameCameras
  dashboardLayout $ do
    $(widgetFile "dashboard")
