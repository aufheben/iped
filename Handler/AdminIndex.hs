module Handler.AdminIndex where

import Import

getAdminIndexR :: Handler Html
getAdminIndexR = do
  pc <- widgetToPageContent $(widgetFile "admin-index")
  withUrlRenderer [hamlet| ^{pageBody pc} |]
