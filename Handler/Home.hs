module Handler.Home where

import Import
import Handler.Util
import Yesod.Auth.Account

-- This is a handler function for the GET request method on the HomeR
-- resource pattern. All of your resource patterns are defined in
-- config/routes
--
-- The majority of the code you will write in Yesod lives in these handler
-- functions. You can spread them across multiple files if you are so
-- inclined, or create a single monolithic file.
getHomeR :: Handler Html
getHomeR = do
  maid <- maybeAuthId
  Just (Entity _ camera1) <- runDB $ getBy $ UniqueDevId "12345678"
  Just (Entity _ camera2) <- runDB $ getBy $ UniqueDevId "12345679"
  defaultLayout $ do
    -- TODO: ideally we should use toWidgetHead and hamletFile to put this
    -- in <head>, but somehow toWidgetHead doesn't work.
    --
    -- This is probably due to the way defaultLayout includes files:
    -- "default-layout" is the contents of the body tag, and
    -- "default-layout-wrapper" is the entire page. The argument `widget` to
    -- defaultLayout is the stuff below, and goes to "default-layout".
    --
    -- But according to http://www.yesodweb.com/book/widgets toWidgetHead should
    -- work anyway.
    --
    -- Also, it's a bit weird that isHomepage can't be is_homepage.
    let isHomepage = True
        streamId1  = cameraStreamId camera1
        dev_id1    = cameraDevId camera1
        streamId2  = cameraStreamId camera2
        dev_id2    = cameraDevId camera2
        navbar     = $(widgetFile "navbar")
        footer     = $(widgetFile "footer")
        -- player     = $(widgetFile "player")
    $(widgetFile "homepage-links")
    $(widgetFile "homepage")
--    (formWidget, formEnctype) <- generateFormPost sampleForm
--    let submission = Nothing :: Maybe (FileInfo, Text)
--        handlerName = "getHomeR" :: Text
--    defaultLayout $ do
--        aDomId <- newIdent
--        setTitle "Welcome To Yesod!"
--        $(widgetFile "homepage")
