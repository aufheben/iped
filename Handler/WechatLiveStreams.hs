{-# LANGUAGE DeriveGeneric #-}

module Handler.WechatLiveStreams where

import Import
import Data.Maybe (fromJust)
import Network.HTTP.Simple
import Wechat (appId, appSecret)
import WechatOrder
import qualified Data.HashMap.Strict as M

-- TODO: move these to Wechat.hs with GHC 8 new feature
data WeAuth = WeAuth { access_token  :: String -- not used for snsapi_base scope
                     , expires_in    :: Int
                     , refresh_token :: String
                     , openid        :: Text
                     , scope         :: Text
                     }
  deriving (Generic, Show)

instance FromJSON WeAuth

authUrl :: String -> String
authUrl code = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" <>
               appId <> "&secret=" <> appSecret <> "&code=" <> code <>
               "&grant_type=authorization_code"

wechatAuthorize :: Handler (Text, Text)
wechatAuthorize = do
  code' <- lookupGetParam "code"
  case code' of
    Nothing   -> do
      -- We are just curious, when this happens, is openid in the session?
      -- Test showed that it is Nothing as well.
      id'    <- lookupSession "openid"
      error $ "A user didn't authorize, user openid: " <> show id'
    Just code -> do
      appid  <- fromJust <$> lookupGetParam "state"
      id'    <- lookupSession "openid"
      $logWarn $ "==> wechatAuthorize, third-party appid: " <> appid <>
                 ", user openid: " <> tshow id'
      openid <- case id' of
                  Nothing -> do
                    req  <- parseUrlThrow (authUrl $ unpack code)
                    resp <- httpJSON req
                    let auth = getResponseBody resp :: WeAuth
                        o_id = openid auth
                    $logDebug (pack $ show auth)
                    setSession "openid" o_id
                    return o_id
                  Just o_id -> return o_id
      return (appid, openid)

getWechatLiveStreamsR :: Handler Html
getWechatLiveStreamsR = do
  (appid, openid) <- wechatAuthorize
  -- return the list of cameras that the user has purchased **under this appid**
  (appname, public_cams, private_cams, orders) <- runDB $ do
    _            <- insertBy $ WechatUser openid
    Entity _ app <- getBy404 $ UniqueAppId appid
    cameras      <- selectList [CameraWechatAppId ==. appid] []
    let appname    = wechatAccountAppName app
        cams       = map entityVal cameras
        (pub, prv) = partition cameraIsPublic cams
    -- select orders paid by the user
    let filters = [WechatOrderOpenId ==. openid, WechatOrderAppId ==. appid,
                   WechatOrderStatus ==. OrderPaid]
    orders <- map entityVal <$> selectList filters [Desc WechatOrderEnd]
    return $ (appname, pub, prv, orders)
  now <- liftIO $ getCurrentTime
  let user_orders = processOrders now orders
  $logDebug $ pack $ show user_orders
  wechatLayout $ do
    setTitle $ toHtml appname
    $(widgetFile "wechat-livestreams")

-- TODO: I have a feeling that this can be done just with a complex query
-- since the orders have already been sorted by descending end time, the idea is to test if
-- a give id is in our selected set
processOrders :: UTCTime -> [WechatOrder] -> M.HashMap Text (Bool, Text)
processOrders now orders = process orders M.empty
  where
  process [] mp     = mp
  process (x:xs) mp =
    let devid = wechatOrderDevId x
    in case M.lookup devid mp of
         Just _  -> process xs mp
         Nothing -> process xs (M.insert devid (now < wechatOrderEnd x, wechatOrderOrderId x) mp)
