module Handler.WechatOrderCheck where

import Import
import Data.Time (addUTCTime)
import Global
import Handler.WechatOrderQuery
import Wechat
import WechatOrder

getWechatOrderCheckR :: Handler ()
getWechatOrderCheckR =
  modifyMVar_ gOrderCheckStarted $ \started ->
    if started
      then return started
      else do
        run_inner_handler <- handlerToIO
        void $ fork $ run_inner_handler $ foreverIO check_orders
        return True
  where
  check_orders = do
    now <- liftIO getCurrentTime
    let ten_min_ago = addUTCTime (-60 * 10) now
    orders <- map entityVal <$> runDB (selectList [WechatOrderStatus ==. OrderCreated, WechatOrderCreated <. ten_min_ago] [])
    mapM (wechatOrderQuery . wechatOrderOrderId) orders
    sleepMin 1
