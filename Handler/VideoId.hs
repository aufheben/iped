module Handler.VideoId where

import Import
import Handler.Util

getVideoIdR :: Text -> Text -> Handler Html
getVideoIdR dev_id streamId = do
  mmsg <- getMessage
  defaultLayout $ do
    let header = $(widgetFile "header")
        footer = $(widgetFile "footer")
        player = $(widgetFile "player")
    $(widgetFile "video-links")
    $(widgetFile "video-id")

postVideoIdR :: Text -> Text -> Handler Html
postVideoIdR = error "Not yet implemented: postVideoIdR"
