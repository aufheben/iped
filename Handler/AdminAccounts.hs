module Handler.AdminAccounts where

import Import
import Handler.Util
import qualified Data.Text as T

getAdminAccountsR :: Handler Html
getAdminAccountsR = do
  pc <- widgetToPageContent $(widgetFile "admin-accounts")
  withUrlRenderer [hamlet| ^{pageBody pc} |]

getAdminAccountInfoR :: Text -> Handler Html
getAdminAccountInfoR username = do
  (maybe_u, dev_ids) <- runDB $ do
    u <- getBy $ UniqueUsername username
    c <- map (cameraDevId . entityVal) <$> selectList [CameraBindUsername ==. username] []
    return (u, c)
  case maybe_u of
    Nothing           -> failureAlert $ username <> "不存在"
    Just (Entity _ u) -> do
      -- TODO: table
      let User _ _ email _ _ _ _ = u
      successAlert $ username <> ": email " <> email <> " cameras " <> T.pack (show dev_ids)
