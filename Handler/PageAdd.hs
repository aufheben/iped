module Handler.PageAdd where

import Import
import Handler.Util
import qualified Data.Text as T

getPageAddR :: Handler Html
getPageAddR = do
  mmsg <- getMessage
  pc   <- widgetToPageContent $(widgetFile "page-add")
  withUrlRenderer [hamlet| ^{pageBody pc} |]

postPageAddR :: Handler Html
postPageAddR = do
  maid <- maybeAuthId
  case maid of
    Nothing       -> redirect $ AuthR LoginR
    Just username -> do
      id_      <- runInputPost $ ireq textField "id"
      password <- runInputPost $ ireq textField "password"
      maybe_c  <- runDB $ getBy $ UniqueDevId id_
      case maybe_c of
        Nothing           -> failureAlert "该设备不存在"
        Just (Entity camera_id c) ->
          case cameraBindUsername c of
            "" -> if encodeUtf8 password == cameraBindPassword c
                    then do
                      runDB $ do
                        cameras <- selectList [CameraBindUsername ==. username] []
                        update camera_id [CameraBindUsername =. username,
                                          CameraDevName  =. "设备" <>
                                            (T.pack . show $ length cameras + 1)
                                         ]
                        -- TODO: also set default name
                      successAlert "设备绑定成功"
                      -- there's no need to delete the binding password, it's not useful anymore
                    else failureAlert "绑定密码不正确"
            _  -> failureAlert "该设备已被绑定"
