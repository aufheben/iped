{-# LANGUAGE DeriveGeneric #-}

module Handler.UpdateStreamStat where

import Import
import Global
import Handler.Util
import Network.HTTP.Simple
import Wechat
import qualified Data.Text as T

data SStat = SStat { name :: Text, send_bytes :: Int, recv_bytes :: Int }
  deriving (Generic, Show)

data Streams = Streams { streams :: [SStat] }
  deriving (Generic, Show)

instance FromJSON SStat
instance FromJSON Streams

getUpdateStreamStatR :: Handler ()
getUpdateStreamStatR = do
  modifyMVar_ gUpdateStreamStatStarted $ \started ->
    if started
      then return started
      else do
        run_inner_handler <- handlerToIO
        void $ fork $ run_inner_handler $ foreverIO update_stream_stat
        return True
  where
  update_stream_stat = do
    $logInfo "updating stream statistics"
    r <- try $ liftIO $ do
      resp <- parseUrlThrow "http://localhost:1985/api/v1/streams/" >>= httpJSON
      return (getResponseBody resp :: Streams)
    case r of
      Right (Streams streams) -> mapM_ update_db streams
      Left (SomeException _)  -> $logInfo "failed to obtain stream statistics from srs server"
    sleepMin 10

  update_db s = runDB $ do
    maybe_c  <- getBy $ UniqueStreamId (name s)
    case maybe_c of
      Nothing -> return ()
      Just (Entity camera_id c) -> do
        t <- liftIO $ getCurrentTime
        let sent_old = cameraStreamSendBytes c
            sent_new = send_bytes s
            sent     = sent_new - sent_old
            cost     = (fromIntegral sent / 1000000000.0) * 10
        update camera_id [ CameraStreamSendBytes =. sent_new
                         , CameraStreamRecvBytes =. recv_bytes s ]
        -- when (sent_old /= 0) $ do
        insert $ StreamStat camera_id t sent cost
        maybe_u  <- getBy $ UniqueUsername (cameraBindUsername c)
        case maybe_u of
          Nothing -> return ()
          Just (Entity k u) -> do
            let balance = userAccountBalance u - cost
            update k [ UserAccountBalance =. balance ]
            when (balance < 0) $ do
              when (cameraStreamEnabled c) $ do
                updateWhere [ CameraBindUsername ==. userUsername u]
                            [ CameraStreamEnabled =. False ]
                r <- try $ do
                  req <- parseUrlThrow . T.unpack $
                           "http://" <> mqttServerAddress <>
                           "/v1/command/" <> cameraDevId c <>
                           "?command=reboot"
                  void $ httpLBS req
                case r :: Either SomeException () of
                  _ -> return ()
