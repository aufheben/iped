{-# LANGUAGE DeriveGeneric #-}

module Handler.WechatCamera where

import Import
import Handler.Util
import Wechat

data WeCamera = WeCamera { dev_id     :: Text
                         , stream_fee :: Int
                         , is_public  :: Bool
                         }
  deriving Generic

data WeCamList = WeCamList { app_id    :: Text
                           , cameras   :: [WeCamera]
                           , admin_key :: Text
                           }
  deriving Generic

instance FromJSON WeCamera
instance FromJSON WeCamList

getWechatCameraR :: Handler Html
getWechatCameraR = error "Not yet implemented: getWechatCameraR"

postWechatCameraR :: Handler Value
postWechatCameraR = do
  a <- requireJsonBody :: Handler WeCamList
  if admin_key a /= adminKey
    then permissionDenied "incorrect admin_key"
    else do
      runDB $ mapM_ (set_appid $ app_id a) (cameras a)
      return $ toValue [("result", "OK")]
  where
  set_appid appid (WeCamera i f p) = do
    maybe_c <- getBy $ UniqueDevId i
    case maybe_c of
      Nothing -> error $ "camera " <> unpack i <> " doesn't exist"
      Just (Entity c_key c) -> do
        let appid' = cameraWechatAppId c
        if not (null appid') && appid' /= appid
          then error "camera belongs to a different appid"
          else update c_key [CameraWechatAppId =. appid,
                             CameraStreamFee =. f,
                             CameraIsPublic =. p]
