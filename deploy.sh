#!/bin/bash

#sed -i s/googleapis/useso/ templates/default-layout-wrapper.hamlet
sed -i s/80.85.85.186:3000/120.26.37.161/ Handler/Util.hs
sed -i s/80.85.85.186/120.26.37.161/ Handler/Util.hs
sed -i s/localhost/www.lighted.com.cn/ Wechat.hs

stack clean
stack exec -- yesod keter
scp iped.keter $WECHAT:/root
