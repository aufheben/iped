module Global.Class where

import Import

class ReadGlobal a where
  readG :: MonadIO m => a v -> m v

instance ReadGlobal IORef where
  readG ref = liftIO $ readIORef ref

instance ReadGlobal MVar where
  readG mvar = liftIO $ readMVar mvar

instance ReadGlobal TVar where
  readG tvar = liftIO $ readTVarIO tvar

class LookupGlobal a where
  lookupG :: (Eq k, Hashable k, MonadIO m) => k -> a (HashMap k v) -> m (Maybe v)

lookup_g :: (Eq k, MonadIO m, Hashable k, ReadGlobal r) =>
            k -> r (HashMap k v) -> m (Maybe v)
lookup_g k g = readG g >>= return . lookup k

instance LookupGlobal IORef where
  lookupG = lookup_g

instance LookupGlobal MVar where
  lookupG = lookup_g

instance LookupGlobal TVar where
  lookupG = lookup_g
