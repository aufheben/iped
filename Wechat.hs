{-# LANGUAGE DeriveGeneric #-}

module Wechat where

import Import
import Control.Exception (AsyncException(ThreadKilled))
import Global
import Network.HTTP.Simple
import Util.Log
import qualified Data.ByteString.Char8 as B

data WeCred = WeCred { access_token :: String, expires_in :: Int }
  deriving (Generic, Show)

instance FromJSON WeCred

bshow :: Show a => a -> ByteString
bshow = B.pack . show

appId, appSecret :: IsString a => a
appId     = "wx4d55e40ca67105d3"
appSecret = "3493007e9252e060824598c334cb6291"

-- Notice that this key is specifically for payment
mchId, payKey :: Text
mchId  = "1349832801"
payKey = "X9rQKHJOqERkS4Mi6RTuniXjzKndCDbD"

-- for IPED admin
adminKey :: Text
adminKey = "cXiDbA3nixidHm93BDqXQ49Tk2MMUfeK"

-- Wechat system makes sure that when the token is refreshed, both the old and new token can
-- be used at the same time for a short period of time.
refreshAccessToken :: IO ()
refreshAccessToken = void . fork $ foreverIO refresh
  where
  refresh = do
    debugM "refreshAccessToken" self "refreshing"
    r <- try $ do
      resp <- parseUrlThrow url >>= httpJSON
      let cred = getResponseBody resp :: WeCred
      atomicWriteIORef gAccessToken (access_token cred) -- TODO: will this leak?
      return cred
    case r of
      Right cred -> sleepSec (expires_in cred `div` 2)
      Left e     -> do
        noticeM "refreshAccessToken" self $ bshow (e :: SomeException)
        sleepSec 60

  url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
        <> appId <> "&secret=" <> appSecret

-- We have to use www.lighted.com.cn here for deployment
localAddr :: String
localAddr = "localhost"

checkOpenOrders :: IO ()
checkOpenOrders = void $ fork $ do
  sleepSec 10 -- wait for the app to start
  req <- parseUrlThrow $ "http://" <> localAddr <> "/wechat/pay/ordercheck"
  void $ httpLBS req

refreshStreamStat :: IO ()
refreshStreamStat = void . fork $ do
  sleepSec 10 -- wait for the app to start
  req <- parseUrlThrow $ "http://" <> localAddr <> "/admin/stream_stat"
  void $ httpLBS req

foreverIO :: (MonadIO m, MonadCatch m, MonadBaseControl IO m) => m () -> m ()
foreverIO io = do
  (Left e) <- try (forever io)
  sleepMin 1 -- in case of an immediate exception in do_refresh
  unless (isThreadKilled e) (foreverIO io)

isThreadKilled :: SomeException -> Bool
isThreadKilled e = fromException e == Just ThreadKilled

sleepSec :: MonadIO m => Int -> m ()
sleepSec n = liftIO $ threadDelay (n * 1000000)

sleepMin :: MonadIO m => Int -> m ()
sleepMin n = sleepSec (n * 60)
