# README

## Deploy to China

Edit templates/default-layout-wrapper.hamlet:

```
-  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">
+  <link rel="stylesheet" type="text/css" href="//fonts.useso.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">
```

## How do I get set up? (Obsolete)

_Program versions_

- GHC: 7.8.4
- yesod-bin: 1.4.3.1

_Cabal sandboxing_

There are 2 sandboxes, one for yesod-bin and one for yesod apps:

```bash
# Note: PostgreSQL and sendmail need a bit of setup
$ pwd
/root/yesod

# This is to install yesod-bin
$ cabal update
$ cabal sandbox init
$ cabal install yesod-bin --max-backjumps=-1 --reorder-goals

# Now add `yesod` to PATH
$ export PATH=/root/yesod/.cabal-sandbox/bin:$PATH

# The second sandbox is common to all yesod apps
$ mkdir common-sandbox
$ cd common-sandbox
$ cabal sandbox init

# Then fetch the source
$ cd ..
$ git clone git@bitbucket.org:aufheben/iped.git
$ cd iped
$ cabal sandbox init --sandbox ../common-sandbox/.cabal-sandbox
$ cabal install -j --enable-tests --max-backjumps=-1 --reorder-goals && yesod devel
```
