{-# LANGUAGE PackageImports #-}
import "iped" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
